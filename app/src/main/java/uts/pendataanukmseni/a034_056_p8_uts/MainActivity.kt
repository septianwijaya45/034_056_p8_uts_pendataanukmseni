package uts.pendataanukmseni.a034_056_p8_uts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import uts.pendataanukmseni.a034_056_p8_uts.Authentication.ResetPassword
import java.util.regex.Pattern

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //Auth Declaration
        auth = FirebaseAuth.getInstance()

        //button
        tvForgotPassword.setOnClickListener(this)
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.tvForgotPassword -> {
                val intent = Intent(this, ResetPassword::class.java).also {
                    startActivity(it)
                }
            }

            R.id.btnLogin ->{
                val email =etEmail.text.toString().trim()
                val password = etPassword.text.toString().trim()

                if(email.isEmpty()){
                    etEmail.error = "Email Harus Diisi!"
                    etEmail.requestFocus()
                    return
                }else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    etEmail.error = "Email Tidak Valid!"
                    etEmail.requestFocus()
                    return
                }

                if(password.isEmpty()){
                    etPassword.error = "Password Harus Diisi!"
                    etPassword.requestFocus()
                    return
                }
                
                loginAction(email, password)
            }
        }
    }

    private fun loginAction(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this){
                if(it.isSuccessful){
                    Intent(this, HomeActivity::class.java).also {
                        it.flags =Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(it)
                    }
                }
                else{
                    Toast.makeText(this, "${it.exception?.message}", Toast.LENGTH_SHORT).show()
                }
            }
    }

    //membuat function untuk membuat kondisi sudah login atau belum
    override fun onStart() {
        super.onStart()
        if(auth.currentUser != null){   //jika user login
            Intent(this,HomeActivity::class.java).also {
                //membuat kondisi untuk auto login dan tidak bisa kembali ke register ataupun login
                // kecuali logout
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        }
    }

}
