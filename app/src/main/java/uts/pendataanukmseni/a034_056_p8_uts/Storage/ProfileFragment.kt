package uts.pendataanukmseni.a034_056_p8_uts.Storage

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import uts.pendataanukmseni.a034_056_p8_uts.Authentication.ChangePasswordDirections

import uts.pendataanukmseni.a034_056_p8_uts.R
import java.io.ByteArrayOutputStream

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment(), View.OnClickListener {

    // membuat request code
    companion object{
        const val REQUEST_CAMERA = 100
    }

    private lateinit var auth: FirebaseAuth     // deskripsikan firebase auth
    private lateinit var imageUri : Uri         //menampung url image

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageView.setOnClickListener(this)
        btnSave.setOnClickListener(this)
        icUnverified.setOnClickListener(this)

        //auth
        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser                 // user akun yang masuk aplikasi
        if(user != null){
            if (user.photoUrl != null){
                Picasso.get().load(user.photoUrl).into(imageView)
            }else{
                Picasso.get().load("https://i.stack.imgur.com/34AD2.jpg").into(imageView)
            }

            etNama.setText(user.displayName)
            etEmail.setText(user.email)

            if(user.isEmailVerified){
                icVerified.visibility = View.VISIBLE
            }else{
                icUnverified.visibility = View.VISIBLE
            }
        }

        //mendeklarasikan ketika edit Email diklik maka akan ke fragment update Email
        etEmail.setOnClickListener {
            val actionUpdateEmail = ProfileFragmentDirections.actionUpdateEmail()
            Navigation.findNavController(it).navigate(actionUpdateEmail)
        }

        //mendeklarasikan ketika edit Password diklik maka akan ke fragment update Password
        txChangePassword.setOnClickListener{
            val actionChangePassword = ProfileFragmentDirections.actionUpdatePassword()
            Navigation.findNavController(it).navigate(actionChangePassword)
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imageView -> {
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->        //jika ivProfile diklik maka akan membuka kamera
                    activity?.packageManager?.let {
                        intent.resolveActivity(it).also {
                            startActivityForResult(intent, 100)
                        }
                    }
                }
            }
            R.id.btnSave -> {
                val user = auth.currentUser                 // user akun yang masuk aplikasi
                val image = when{
                    ::imageUri.isInitialized  ->imageUri       //jika gambar sudah diinisialisasi atau diupload
                    //jika gambar tidak ada
                    user?.photoUrl == null -> Uri.parse("https://i.stack.imgur.com/34AD2.jpg")
                    else -> user.photoUrl                      //jika gambar sudah ada dalam database
                }

                val name = etNama.text.toString().trim()
                if(name.isEmpty()){
                    etNama.error = "Nama Harus Diisi!"
                    etNama.requestFocus()
                    return
                }

                UserProfileChangeRequest.Builder()
                    .setDisplayName(name)
                    .setPhotoUri(image)
                    .build()
                    .also {
                        user?.updateProfile(it)?.addOnCompleteListener {
                            if(it.isSuccessful){
                                Toast.makeText(activity,"Profil Berhasil diUpdate", Toast.LENGTH_SHORT).show()
                            }else{
                                Toast.makeText(activity, "${it.exception?.message}", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
            }
            R.id.icUnverified -> {
                val user = auth.currentUser
                user?.sendEmailVerification()?.addOnCompleteListener {
                    if (it.isSuccessful){
                        Toast.makeText(activity, "Verification Email Telah Dikirim!", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(activity, "${it.exception?.message}", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    //    untuk memberikan action ketika memberikan kamera
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK){   // kondisi jika mengupload foto
            val imgBitmap = data?.extras?.get("data")                   //menjadikan foto ke bitmap
            uploadImage(imgBitmap as Bitmap?)                                      // bitmap nanti akan disimpan ke db Firebase
        }
    }

    private fun uploadImage(imgBitmap: Bitmap?) {
        val baos = ByteArrayOutputStream()          //jadikan array
        val ref = FirebaseStorage.getInstance().reference.child("img/${
        FirebaseAuth.getInstance().currentUser?.uid
        }")       //simpan gambar ke dalam folder berdasarkan id user
        if (imgBitmap != null) {
            imgBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        }   //Compress gambar menjadi JPEG dengan quality 100
        val image = baos.toByteArray()      // jadikan baos ke byte array

        ref.putBytes(image)
            .addOnCompleteListener{
                if(it.isSuccessful){
                    //ambil gambar berdasarkan url
                    ref.downloadUrl.addOnCompleteListener {
                        //jika data tidak null
                        it.result?.let{
                            imageUri = it
                            imageView.setImageBitmap(imgBitmap)
                        }
                    }
                }
            }
    }

}
