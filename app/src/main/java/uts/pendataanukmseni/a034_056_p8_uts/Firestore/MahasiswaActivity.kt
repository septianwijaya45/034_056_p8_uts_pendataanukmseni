package uts.pendataanukmseni.a034_056_p8_uts.Firestore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import kotlinx.android.synthetic.main.activity_mahasiswa.*
import kotlinx.android.synthetic.main.row_data_mhs.view.*
import uts.pendataanukmseni.a034_056_p8_uts.R

class MahasiswaActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "mahasiswa"
    val F_NIM = "id"
    val F_NAMA ="nama"
    val F_PRODI = "prodi"
    val F_KELAS = "kelas"
    val F_PHONE = "nomor"
    val F_DIVISI = "devisi"
    var docID = ""
    lateinit var db : FirebaseFirestore
    lateinit var alMhs  : ArrayList<HashMap<String, Any>>
    lateinit var adapter: SimpleAdapter
    lateinit var v : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mahasiswa)

        alMhs = ArrayList()
        btnTambah.setOnClickListener(this)
        btnEdit.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        lsTambahMhs.setOnItemClickListener(itemClick)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambah -> {
                val hm = HashMap<String, Any>()
                hm.set(F_NIM, etNim.text.toString())
                hm.set(F_NAMA, etNama.text.toString())
                hm.set(F_PRODI, etProdi.text.toString())
                hm.set(F_KELAS, etKelas.text.toString())
                hm.set(F_PHONE, etTelepon.text.toString())
                hm.set(F_DIVISI, etDivisi.text.toString())
                db.collection(COLLECTION).document(etNim.text.toString()).set(hm).addOnSuccessListener {
                    etNim.setText("")
                    etNama.setText("")
                    etProdi.setText("")
                    etKelas.setText("")
                    etTelepon.setText("")
                    etDivisi.setText("")
                    Toast.makeText(this, "Data Successfully Added", Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data Unsuccessfully Added : ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnEdit -> {
                val hm = HashMap<String, Any>()
                hm.set(F_NIM, etNim.text.toString())
                hm.set(F_NAMA, etNama.text.toString())
                hm.set(F_PRODI, etProdi.text.toString())
                hm.set(F_KELAS, etKelas.text.toString())
                hm.set(F_PHONE, etTelepon.text.toString())
                hm.set(F_DIVISI, etDivisi.text.toString())
                db.collection(COLLECTION).document(docID).update(hm)
                    .addOnSuccessListener {
                        etNim.setText("")
                        etNama.setText("")
                        etProdi.setText("")
                        etKelas.setText("")
                        etTelepon.setText("")
                        etDivisi.setText("")
                        Toast.makeText(this, "Data Successfully Updated", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(this, "Data Unsuccessfully Updated : ${e.message}", Toast.LENGTH_SHORT).show()
                    }
            }
            R.id.btnDelete -> {
                db.collection(COLLECTION).whereEqualTo(F_NIM, docID).get()
                    .addOnSuccessListener {
                            result ->
                        for (doc : QueryDocumentSnapshot in result){
                            db.collection(COLLECTION).document(doc.id).delete()
                                .addOnSuccessListener {
                                    etNim.setText("")
                                    etNama.setText("")
                                    etProdi.setText("")
                                    etKelas.setText("")
                                    etTelepon.setText("")
                                    etDivisi.setText("")
                                    Toast.makeText(this, "Data Successfully Delete", Toast.LENGTH_SHORT).show()
                                }
                                .addOnFailureListener { e ->
                                    Toast.makeText(this, "Data Unsuccessfully Delete : ${e.message}",
                                        Toast.LENGTH_SHORT).show()
                                }
                        }
                    }.addOnFailureListener{e ->
                        Toast.makeText(this, "Can't get data references ${e.message}",
                            Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm :HashMap<String, Any> = alMhs.get(position)
        docID = hm.get(F_NIM).toString()
        etNim.setText(docID)
        etNama.setText(hm.get(F_NAMA).toString())
        etProdi.setText(hm.get(F_PRODI).toString())
        etKelas.setText(hm.get(F_KELAS).toString())
        etTelepon.setText(hm.get(F_PHONE).toString())
        etDivisi.setText(hm.get(F_DIVISI).toString())
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener {querySnapshot, e ->
            if (e != null) Log.d("fireStore",e.localizedMessage)
            showData()
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alMhs.clear()
            for (doc: QueryDocumentSnapshot in result){
                val hm = HashMap<String, Any>()
                hm.set(F_NIM, doc.get(F_NIM).toString())
                hm.set(F_NAMA, doc.get(F_NAMA).toString())
                hm.set(F_PRODI, doc.get(F_PRODI).toString())
                hm.set(F_KELAS, doc.get(F_KELAS).toString())
                hm.set(F_DIVISI, doc.get(F_DIVISI).toString())
                hm.set(F_PHONE, doc.get(F_PHONE).toString())
                alMhs.add(hm)
            }
            adapter = SimpleAdapter(this,alMhs,R.layout.row_data_mhs,
                arrayOf(F_NIM,F_NAMA,F_PRODI, F_KELAS, F_DIVISI, F_PHONE),
                intArrayOf(R.id.tvNim, R.id.tvNama, R.id.tvProdi, R.id.tvKelas, R.id.tvDevisi, R.id.tvTelpon))
            lsTambahMhs.adapter = adapter
        }
    }
}
