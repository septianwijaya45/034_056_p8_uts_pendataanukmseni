package uts.pendataanukmseni.a034_056_p8_uts.Firestore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import kotlinx.android.synthetic.main.activity_devisi.*
import uts.pendataanukmseni.a034_056_p8_uts.R

class DevisiActivity : AppCompatActivity(), View.OnClickListener {
    val COLLECTION = "devisi"
    val F_NAME ="nama_devisi"
    var docID = ""
    lateinit var db : FirebaseFirestore

    lateinit var alDevisi  : ArrayList<HashMap<String, Any>>
    lateinit var adapter: SimpleAdapter
    lateinit var v : View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_devisi)

        alDevisi = ArrayList()
        btnTambahDivisi.setOnClickListener(this)
        btnEditDivisi.setOnClickListener(this)
        btnDeleteDivisi.setOnClickListener(this)
        lsTambahDevisi.setOnItemClickListener(itemClick)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambahDivisi  ->{
                val hm = HashMap<String, Any>()
                hm.set(F_NAME, etDivisi.text.toString())
                db.collection(COLLECTION).document(etDivisi.text.toString()).set(hm).addOnSuccessListener {
                    etDivisi.setText("")
                    Toast.makeText(this, "Data Successfully Added", Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data Unsuccessfully Added : ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnEditDivisi  ->{
                val hm = HashMap<String, Any>()
                hm.set(F_NAME, etDivisi.text.toString())
                db.collection(COLLECTION).document(docID).update(hm)
                    .addOnSuccessListener {
                        etDivisi.setText("")
                        Toast.makeText(this, "Data Successfully Updated", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(this, "Data Unsuccessfully Updated : ${e.message}", Toast.LENGTH_SHORT).show()
                    }
            }
            R.id.btnDeleteDivisi  ->{
                db.collection(COLLECTION).whereEqualTo(F_NAME, docID).get()
                    .addOnSuccessListener {
                            result ->
                        for (doc : QueryDocumentSnapshot in result){
                            db.collection(COLLECTION).document(doc.id).delete()
                                .addOnSuccessListener {
                                    etDivisi.setText("")
                                    Toast.makeText(this, "Data Successfully Delete", Toast.LENGTH_SHORT).show()
                                }
                                .addOnFailureListener { e ->
                                    Toast.makeText(this, "Data Unsuccessfully Delete : ${e.message}",
                                        Toast.LENGTH_SHORT).show()
                                }
                        }
                    }.addOnFailureListener{e ->
                        Toast.makeText(this, "Can't get data references ${e.message}",
                            Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm :HashMap<String, Any> = alDevisi.get(position)
        docID = hm.get(F_NAME).toString()
        etDivisi.setText(docID)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener {querySnapshot, e ->
            if (e != null) Log.d("fireStore",e.localizedMessage)
            showData()
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alDevisi.clear()
            for (doc: QueryDocumentSnapshot in result){
                val hm = HashMap<String, Any>()
                hm.set(F_NAME, doc.get(F_NAME).toString())
                alDevisi.add(hm)
            }
            adapter = SimpleAdapter(this,alDevisi,R.layout.row_data_devisi,
                arrayOf(F_NAME),
                intArrayOf(R.id.tvNamaDev))
            lsTambahDevisi.adapter = adapter
        }
    }


}
