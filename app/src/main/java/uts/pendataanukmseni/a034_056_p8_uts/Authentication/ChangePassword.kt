package uts.pendataanukmseni.a034_056_p8_uts.Authentication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import kotlinx.android.synthetic.main.fragment_change_password.*

import uts.pendataanukmseni.a034_056_p8_uts.R

/**
 * A simple [Fragment] subclass.
 */
class ChangePassword : Fragment(), View.OnClickListener {
    private lateinit var auth : FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //deklarasikan Firebase Auth
        auth = FirebaseAuth.getInstance()

        //variabel ambil data user
        val user = auth.currentUser

        //Deklarasikan Layout View
        layoutPassword.visibility = View.VISIBLE        // Tampilkan Password
        layoutNewPassword.visibility = View.GONE              // Sembunyikan Password

        // Button
        btnAuth.setOnClickListener(this)
        // Jika Password Benar, Maka btnUpdate harus diberikan update
        btnUpdate.setOnClickListener { view ->
            val newPassword = etNewPassword.text.toString().trim()
            val newPasswordConfirm = etNewPasswordConfirm.text.toString().trim()

            if (newPassword.isEmpty() || newPassword.length < 6) {        //memberikan kondisi jika email kosong
                etNewPassword.error = "Password harus lebih dari 6!"
                etNewPassword.requestFocus()
                return@setOnClickListener
            }

            if (newPassword != newPasswordConfirm) {
                etNewPasswordConfirm.error = "Password Tidak Sama!"
                etNewPasswordConfirm.requestFocus()
                return@setOnClickListener
            }

            user?.let {
                user.updatePassword(newPassword).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val actionPasswordChange =
                            ChangePasswordDirections.actionPasswordUpdated()
                        Navigation.findNavController(view).navigate(actionPasswordChange)
                        Toast.makeText(activity, "Password Berhasil Di Update!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(activity, "${it.exception?.message}", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }
    }

    override fun onClick(v: View?) {
        val user = auth.currentUser
        when (v?.id) {
            R.id.btnAuth -> {
                val password = etPassword.text.toString()
                if (password.isEmpty()) {    //jika password tidak diisi
                    etPassword.error = "Password Harus Diisi!"
                    etPassword.requestFocus()
                    return
                }


                user?.let {
                    // Adanya email pada password
                    val userCredential = EmailAuthProvider.getCredential(it.email!!, password)
                    it.reauthenticate(userCredential).addOnCompleteListener {
                        if (it.isSuccessful) {
                            layoutPassword.visibility =
                                View.GONE       //Jika Password Benar, maka Hide LayoutPassword
                            layoutNewPassword.visibility =
                                View.VISIBLE       //Tampilkan Layout Email
                        } else if (it.exception is FirebaseAuthInvalidCredentialsException) {
                            etPassword.error =
                                "Password Tidak Valid"   //Jika Password Salah, Tampilkan Pesan Error
                            etPassword.requestFocus()
                        } else {
                            Toast.makeText(activity, "${it.exception?.message}", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }
    }
}
