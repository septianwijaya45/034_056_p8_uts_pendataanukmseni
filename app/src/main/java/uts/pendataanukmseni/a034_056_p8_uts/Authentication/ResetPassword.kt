package uts.pendataanukmseni.a034_056_p8_uts.Authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_reset_password.*
import uts.pendataanukmseni.a034_056_p8_uts.HomeActivity
import uts.pendataanukmseni.a034_056_p8_uts.MainActivity
import uts.pendataanukmseni.a034_056_p8_uts.R

class ResetPassword : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        btnBack.setOnClickListener(this)
        btnResetPassword.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnBack -> {
                Intent(this, MainActivity::class.java).also {
                    startActivity(it)
                }
            }
            R.id.btnResetPassword -> {
                val email = etEmailReset.text.toString().trim()

                if(email.isEmpty()){
                    etEmailReset.error = "Email Harus Diisi!"
                    etEmailReset.requestFocus()
                    return
                }

                if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    etEmailReset.error = "Email Tidak Falid"
                    etEmailReset.requestFocus()
                    return
                }

                FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener{
                    if(it.isSuccessful){
                        Toast.makeText(this, "Cek Email untuk Mengganti Password", Toast.LENGTH_SHORT).show()
                        //Jika Berhasil Maka Akan Kembali Ke Halaman Login
                        Intent(this, MainActivity::class.java).also {
                            startActivity(it)
                        }
                    }else{
                        Toast.makeText(this, "${it.exception?.message}", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}
