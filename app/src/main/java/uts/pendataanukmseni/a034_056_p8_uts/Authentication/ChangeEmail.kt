package uts.pendataanukmseni.a034_056_p8_uts.Authentication

import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_change_email.*
import kotlinx.android.synthetic.main.fragment_change_email.etEmail
import kotlinx.android.synthetic.main.fragment_change_email.etPassword

import uts.pendataanukmseni.a034_056_p8_uts.R

/**
 * A simple [Fragment] subclass.
 */
class ChangeEmail : Fragment(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser

        // Button
        btnAuth.setOnClickListener(this)
        // Jika Password Benar, Maka btnUpdate harus diberikan update
        btnUpdate.setOnClickListener{view ->
            val email = etEmail.text.toString().trim()

            if(email.isEmpty()){        //memberikan kondisi jika email kosong
                etEmail.error = "Email must be entered!"
                etEmail.requestFocus()
                return@setOnClickListener
            }

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){ //untuk mengecek email valid atau tidak
                etEmail.error = "Email not valid!"
                etEmail.requestFocus()
                return@setOnClickListener
            }

            user?.let {
                user.updateEmail(email).addOnCompleteListener{
                    if (it.isSuccessful){
                        val actionEmailUpdated = ChangeEmailDirections.actionEmailUpdated()
                        Navigation.findNavController(view).navigate(actionEmailUpdated)
                        Toast.makeText(activity, "Sukses Update Email", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(activity, "${it.exception?.message}", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        //Layout View
        layoutPassword.visibility = View.VISIBLE        // Tampilkan Password
        layoutEmail.visibility = View.GONE              // Sembunyikan Password
    }

    override fun onClick(v: View?) {
        val user = auth.currentUser
        when(v?.id){
            R.id.btnAuth -> {
                val password = etPassword.text.toString()
                if (password.isEmpty()){    //jika password tidak diisi
                    etPassword.error = "Password Harus Diisi!"
                    etPassword.requestFocus()
                    return
                }

                user?.let{
                    // Adanya email pada password
                    val userCredential = EmailAuthProvider.getCredential(it.email!!, password)
                    it.reauthenticate(userCredential).addOnCompleteListener{
                        if (it.isSuccessful){
                            layoutPassword.visibility = View.GONE       //Jika Password Benar, maka Hide LayoutPassword
                            layoutEmail.visibility = View.VISIBLE       //Tampilkan Layout Email
                        }else if(it.exception is FirebaseAuthInvalidCredentialsException){
                            etPassword.error = "Password Tidak Valid"   //Jika Password Salah, Tampilkan Pesan Error
                            etPassword.requestFocus()
                        }else{
                            Toast.makeText(activity, "${it.exception?.message}", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

}
