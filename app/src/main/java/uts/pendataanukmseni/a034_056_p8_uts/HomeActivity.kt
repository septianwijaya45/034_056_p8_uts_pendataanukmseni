package uts.pendataanukmseni.a034_056_p8_uts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_home.*
import uts.pendataanukmseni.a034_056_p8_uts.Firestore.DevisiActivity
import uts.pendataanukmseni.a034_056_p8_uts.Firestore.MahasiswaActivity

class HomeActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        //Button

        //Menu Action
        val navController = findNavController(R.id.nav_host_fragment) //panggil fragment pada activity_home.xml
        val appBarConfiguration = AppBarConfiguration.Builder(        //memanggil home, mahasiswa, devisi dan profile pada menu
            R.id.nav_home, R.id.nav_profile
        ).build()

        // Menampilkan Navigation Button
        nav_bottom.setupWithNavController(navController)

        //Deklarasi Ambil Data Auth
        auth = FirebaseAuth.getInstance()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.btnLogOut -> {
                auth.signOut()
                Intent(this, MainActivity::class.java).also {
                    //membuat kondisi untuk auto login dan tidak bisa kembali ke register ataupun login
                    // kecuali logout
                    it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(it)
                }
                return true
            }
            R.id.btnTambahMhs-> {
                Intent(this, MahasiswaActivity::class.java).also{
                    startActivity(it)
            }
                return true
        }

            R.id.btnTambahDevisi-> {
                Intent(this, DevisiActivity::class.java).also{
                    startActivity(it)
                }
                return true
            }
            else -> return true
        }
    }

}
